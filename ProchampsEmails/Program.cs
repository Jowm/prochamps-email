﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace ProchampsEmails
{
    class Program
    {
        static void Main(string[] args)
        {

            //Prochamps();
            DownloadEmail();
        }

        public static void DownloadEmail()
        {

            clsConnection cls = new clsConnection();
            Console.WriteLine(DateTime.Now + ": " + "Create Outlook application.");

            Outlook.Application oApp = new Outlook.Application();

            Console.WriteLine(DateTime.Now + ": " + "Get Mapi NameSpace.");

            Outlook.NameSpace oNS = oApp.GetNamespace("MAPI");

            //string displayName = oNS.DefaultStore.DisplayName;
            string displayName = "RusselBruce.Cipriano@altisource.com";

            Console.WriteLine(DateTime.Now + ": " + "Get Messages collection of Inbox.");

            Outlook.MAPIFolder oInbox = oNS.Folders["upsdelnotif"].Folders["Inbox"];
            Outlook.MAPIFolder oConfirmed = oNS.Folders["upsdelnotif"].Folders["Confirmed"];

            Outlook.Items oItems = oInbox.Items;
            Outlook.MailItem oMsg = default(Outlook.MailItem);

            string[] array = { };

            string query = "select * from tbl_VPR_ShippingUI_WI where isSubmit = 2 and isConfirm is null";
            DataTable dt = cls.GetData(query);
            DateTime dTime = DateTime.Now;

            if (dt.Rows.Count > 0)
            {
                string qryUpdate = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    oItems = oInbox.Items.Restrict("@SQL=\"urn:schemas:httpmail:subject\" like '%" + dt.Rows[i]["Tracking"].ToString() + "%'");
                    Console.WriteLine(DateTime.Now + ": Tracking Number: " + dt.Rows[i]["Tracking"].ToString());
                    Console.WriteLine(DateTime.Now + ": Total email count: " + oItems.Count);
                    Console.WriteLine(DateTime.Now + ": Looping all emails...");


                    //String savepath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\" + filename + ".msg";

                    for (int x = 1; x <= oItems.Count; x++)
                    {
                        oMsg = oItems[x];


                        String filename = dt.Rows[i]["Tracking"].ToString();

                        //string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

                        //foreach (char c in invalid)
                        //{
                        //    filename = filename.Replace(c.ToString(), "");
                        //}
                        String savepath = @"C:\Users\ciprianr\Desktop\Emails" + @"\" + filename + ".msg";

                        oMsg.SaveAs(savepath);
                        oMsg.Move(oConfirmed);
                        //if (oMsg.Subject.IndexOf("FW: RP Registration request on - ") > -1)
                        //{
                        //    string proid = oMsg.Subject.Replace("FW: RP Registration request on - ", "");

                        //    string update = "update tbl_VPR_CreatedFile set [Status] = '', [Date Verified] = GETDATE() where [PRO ID] = '" + proid + "' ";

                        //    update += "and [Status] = 'Pending' and Removed is null and [Done Outside Software] is null and [Prochamp Status] not in ('Unlocked by Prochamps', 'Reviewed by Prochamps')";

                        //    int exec = cls.ExecuteQuery(update);

                        //    if (exec > 0)
                        //    {
                        //        //oMsg.Move(oVerified);
                        //        Console.WriteLine("Verified: " + proid);
                        //    }
                        //    //Console.WriteLine("Verified: " + proid);
                        //}
                        qryUpdate += "update tbl_VPR_ShippingUI_WI set isConfirm = true,confirm_date = '" + DateTime.Now + "',confirm_filename = '" + filename + "' where Tracking = '" + dt.Rows[i]["Tracking"].ToString() + "';";
                    }

                    

                }
                int exec = cls.ExecuteQuery(qryUpdate);
            }
           

            TimeSpan totalMins = DateTime.Now.Subtract(dTime);

            Console.WriteLine("Total minutes: " + totalMins);

            Console.WriteLine("Done!");

            Environment.Exit(0);

        }

        public static void Prochamps()
        {
            //TEST LANG

            string mailbox = "VPR";
            int ecnt = 0;
            int ec = 0;
            int ea = 0;

            Console.WriteLine(DateTime.Now + ": " + "Create Outlook application.");

            Outlook.Application oApp = new Outlook.Application();

            Console.WriteLine(DateTime.Now + ": " + "Get Mapi NameSpace.");

            Outlook.NameSpace oNS = oApp.GetNamespace("MAPI");

            //string displayName = oNS.DefaultStore.DisplayName;
            string displayName = "RusselBruce.Cipriano@altisource.com";

            Console.WriteLine(DateTime.Now + ": " + "Get Messages collection of Inbox.");

            Outlook.MAPIFolder oInbox = oNS.Folders["Public Folders - " + displayName].Folders["All Public Folders"].Folders["VPR"].Folders["PROCHAMPS"];
            Outlook.MAPIFolder oVerified = oNS.Folders["Public Folders - " + displayName].Folders["All Public Folders"].Folders["VPR"].Folders["PROCHAMPS"].Folders["Verified Emails"];

            Console.WriteLine(DateTime.Now + ": " + mailbox + " inbox");

            Outlook.Items oItems = oInbox.Items;

            oItems = oInbox.Items.Restrict("@SQL=\"urn:schemas:httpmail:subject\" like '%FW: RP Registration request on - %'");

            Console.WriteLine(DateTime.Now + ": Total email count: " + oItems.Count);
            ecnt = oItems.Count;

            Outlook.MailItem oMsg = default(Outlook.MailItem);

            ec = 0;
            ea = 0;

            ecnt = oItems.Count;

            ArrayList proIDList = new ArrayList();

            clsConnection cls = new clsConnection();
            string asd = "";

            Console.WriteLine(DateTime.Now + ": Looping all emails...");
            DateTime dTime = DateTime.Now;

            for (int x = 1; x <= oItems.Count; x++)
            {
                oMsg = oItems[x];

                //if (!proIDList.Contains(oMsg.Subject))
                //{
                //    proIDList.Add(oMsg.Subject.Replace("RE: RP Registration request on - ", ""));

                //    asd += "'" + oMsg.Subject.Replace("RE: RP Registration request on - ", "") + "',\n";
                //}         

                if (oMsg.Subject.IndexOf("FW: RP Registration request on - ") > -1)
                {
                    string proid = oMsg.Subject.Replace("FW: RP Registration request on - ", "");

                    string update = "update tbl_VPR_CreatedFile set [Status] = '', [Date Verified] = GETDATE() where [PRO ID] = '" + proid + "' ";

                    update += "and [Status] = 'Pending' and Removed is null and [Done Outside Software] is null and [Prochamp Status] not in ('Unlocked by Prochamps', 'Reviewed by Prochamps')";

                    int exec = cls.ExecuteQuery(update);

                    if (exec > 0)
                    {
                        oMsg.Move(oVerified);
                        Console.WriteLine("Verified: " + proid);
                    }
                    //Console.WriteLine("Verified: " + proid);
                }
            }

            TimeSpan totalMins = DateTime.Now.Subtract(dTime);

            Console.WriteLine("Total minutes: " + totalMins);

            Console.WriteLine("Done!");

            Environment.Exit(0);
        }
    }
}
